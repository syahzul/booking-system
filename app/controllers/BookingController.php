<?php

class BookingController extends \BaseController {

	public function index()
	{
		$bookings = Booking::all();
		return View::make('bookings.index', compact('bookings'));
	}

	public function create()
	{
		$rooms = Room::lists('name', 'id');
		return View::make('bookings.create', compact('rooms'));
	}

	public function store()
	{
		$input = Input::only(array(
			'started_at', 'ended_at', 'room_id', 'name',
			'email', 'telephone', 'published'
		));

		$rules = array(
			'started_at' => 'required|date',
			'ended_at'   => 'required|date',
			'room_id'    => 'required|exists:rooms,id',
			'name'       => 'required',
			'email'      => 'required|email',
		);

		$validation = Validator::make($input, $rules);

		if ($validation->passes()) {
			Booking::create($input);
			return Redirect::route('booking.index');
		}
		else {
			return Redirect::back()->withErrors($validation)->withInput();
		}
	}

	public function edit($id)
	{
		$booking = Booking::find($id);
		$rooms = Room::lists('name', 'id');
		return View::make('bookings.edit', compact('booking', 'rooms'));
	}

	public function update($id)
	{
		$input = Input::only(array(
			'started_at', 'ended_at', 'room_id', 'name',
			'email', 'telephone', 'published'
		));

		$booking = Booking::find($id);

		$rules = array(
			'started_at' => 'required|date',
			'ended_at'   => 'required|date',
			'room_id'    => 'required|exists:rooms,id',
			'name'       => 'required',
			'email'      => 'required|email',
		);

		$validation = Validator::make($input, $rules);

		if ($validation->passes()) {
			$booking->update($input);
			return Redirect::route('booking.index');
		}
		else {
			return Redirect::back()->withErrors($validation)->withInput();
		}
	}

	public function destroy($id)
	{
		$booking = Booking::find($id);
		$booking->delete();
		return Redirect::route('booking.index');
	}

}
