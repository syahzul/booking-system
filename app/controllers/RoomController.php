<?php

class RoomController extends \BaseController {

	public function index()
	{
		$rooms = Room::orderBy('name', 'asc')->get();

		return View::make('rooms.index', compact('rooms'));
	}

	public function create()
	{
		return View::make('rooms.create');
	}

	public function store()
	{
		$input = Input::only(array('name', 'picture'));
		$rules = array(
			'name'    => 'required|unique:rooms,name',
			'picture' => 'image'
		);

		$validation = Validator::make($input, $rules);

		if ($validation->passes()) {

			// do we need to upload an image?
			if (Input::hasFile('picture')) {

				// generate unique name
				$name = md5(microtime()).'.'.$input['picture']->getClientOriginalExtension();

				// upload the image using new name
				$input['picture']->move(public_path().'/images', $name);

				// change the input value
				$input['picture'] = $name;
			}

			// create the record
			Room::create($input);

			// back to index page
			return Redirect::route('room.index');
		}
		else {
			return Redirect::back()->withErrors($validation)->withInput();
		}
	}

	public function edit($id)
	{
		$room = Room::find($id);
		return View::make('rooms.edit', compact('room'));
	}

	public function update($id)
	{
		$input = Input::only(array('name', 'picture'));
		$rules = array(
			'name'    => 'required|unique:rooms,name,'.$id,
			'picture' => 'image'
		);

		$validation = Validator::make($input, $rules);

		if ($validation->passes()) {

			// get the item to modified
			$room = Room::find($id);

			// do we need to upload an image?
			if (Input::hasFile('picture')) {

				// do we have image uploded before?
				if (File::exists(public_path().'/images/'.$room->picture)) {

					// delete the file
					File::delete(public_path().'/images/'.$room->picture);
				}

				// generate unique name
				$name = md5(time()).'.'.$input['picture']->getClientOriginalExtension();

				// upload the image using new name
				$input['picture']->move(public_path().'/images', $name);

				// change the input value
				$input['picture'] = $name;
			}

			// update the record
			$room->update($input);

			// back to index page
			return Redirect::route('room.index');
		}
		else {
			return Redirect::route('room.edit', $id)->withErrors($validation)->withInput();
		}
	}

	public function destroy($id)
	{
		$room = Room::find($id);

		if ($room->bookings()->count()) {

			foreach ($room->bookings as $booking) {
				$booking->delete();
			}

		}

		$room->delete();
		return Redirect::route('room.index');
	}

}
