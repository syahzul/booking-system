<?php

class UserController extends BaseController {

	public function getRegister()
	{
		return View::make('users.register');
	}

	public function postRegister()
	{
		$input = Input::except(array('_token', '_method'));

		$rules = array(
			'name' => 'required',
			'email' => 'required|email|unique:users,email',
			'password' => 'required|confirmed|min:8',
		);

		$validation = Validator::make($input, $rules);

		if ($validation->fails()) {
			return Redirect::back()->withInput()->withErrors($validation);
		}

		User::create($input);

		return Redirect::to('/login');
	}

	public function getLogin()
	{
		return View::make('users.login');
	}

	public function postLogin()
	{
		$input = Input::only(array('email', 'password'));

		$rules = array(
			'email' => 'required|email',
			'password' => 'required|min:8'
		);

		$validation = Validator::make($input, $rules);

		if ($validation->fails()) {
			return Redirect::back()->withInput()->withErrors($validation);
		}

		if (Auth::attempt($input)) {
			return Redirect::to('/booking');
		}

		return Redirect::to('/login')->withError('Login failed!');
	}

	public function getLogout()
	{
		Auth::logout();
		return Redirect::to('/login');
	}

	public function getIndex()
	{
		$users = User::orderBy('name')->get();
		return View::make('users.index', compact('users'));
	}

	public function getCreate()
	{
		return View::make('users.create');
	}

	public function postCreate()
	{
		$input = Input::except(array('_token', '_method'));

		$rules = array(
			'name' => 'required',
			'email' => 'required|email|unique:users,email',
			'password' => 'required|confirmed|min:8',
		);

		$validation = Validator::make($input, $rules);

		if ($validation->fails()) {
			return Redirect::back()->withInput()->withErrors($validation);
		}

		User::create($input);

		return Redirect::to('/user');
	}

	public function getEdit($id)
	{
		$user = User::findOrFail($id);
		return View::make('users.edit', compact('user'));
	}

	public function patchUpdate($id)
	{
		$input = Input::except(array('_token', '_method'));

		$rules = array(
			'name' => 'required',
			'email' => 'required|email|unique:users,email,'.$id,
			'password' => 'confirmed|min:8',
		);

		if (empty($input['password'])) {
			unset($input['password']);
		}

		$validation = Validator::make($input, $rules);

		if ($validation->fails()) {
			return Redirect::back()->withInput()->withErrors($validation);
		}

		$user = User::findOrFail($id);
		$user->update($input);

		return Redirect::to('/user');
	}

	public function delete($id)
	{
		$user = User::findOrFail($id);
		$user->delete();

		return Redirect::to('/user');
	}
}