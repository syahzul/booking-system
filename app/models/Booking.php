<?php

class Booking extends Eloquent
{
	protected $fillable = array(
		'started_at', 'ended_at', 'room_id', 'name',
		'email', 'telephone', 'published'
	);

	public function room()
	{
		return $this->belongsTo('Room');
	}

}