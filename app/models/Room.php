<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Room extends Eloquent
{
	use SoftDeletingTrait;

	protected $dates = ['deleted_at'];

	protected $fillable = array('name', 'picture');
	public $timestamps = false;

	public function bookings()
	{
		return $this->hasMany('Booking');
	}
	
}