<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('home');
});


Route::group(['before' => 'auth'], function() {

	Route::resource('room', 'RoomController');
	Route::resource('booking', 'BookingController');

});

Route::get('/register', 'UserController@getRegister');
Route::post('/register', 'UserController@postRegister');

Route::get('/login', 'UserController@getLogin');
Route::post('/login', 'UserController@postLogin');

Route::get('/logout', 'UserController@getLogout');

Route::controller('password', 'RemindersController');

Route::get('/user', 'UserController@getIndex');
Route::get('/user/create',
	[
		'uses' => 'UserController@getCreate',
		'as' => 'user.create',
	]
);
Route::post('/user/create',
	[
		'uses' => 'UserController@postCreate',
		'as' => 'user.store',
	]
);
Route::get('/user/{user}/edit',
	[
		'uses' => 'UserController@getEdit',
		'as' => 'user.edit',
	]
);
Route::patch('/user/{user}/edit',
	[
		'uses' => 'UserController@patchUpdate',
		'as' => 'user.update',
	]
);
Route::delete('/user/{user}/delete',
	[
		'uses' => 'UserController@delete',
		'as' => 'user.destroy',
	]
);
