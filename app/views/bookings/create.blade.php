@extends('master')

@section('content')

	<div class="page-header">
		<h1>Add New Booking</h1>
	</div>

	{{ Form::open(array('route' => 'booking.store', 'class' => 'form-horizontal', 'files' => true)) }}
		
		<div class="form-group">
			{{ Form::label('started_at', 'Started at', array('class' => 'control-label col-sm-2')) }}
			<div class="col-xs-3">
				{{ Form::text('started_at', null, array('class' => 'form-control')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('ended_at', 'Ended at', array('class' => 'control-label col-sm-2')) }}
			<div class="col-xs-3">
				{{ Form::text('ended_at', null, array('class' => 'form-control')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('room_id', 'Room', array('class' => 'control-label col-sm-2')) }}
			<div class="col-sm-4">
				{{ Form::select('room_id', $rooms, null, array('class' => 'form-control')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('name', 'Name', array('class' => 'control-label col-sm-2')) }}
			<div class="col-sm-4">
				{{ Form::text('name', null, array('class' => 'form-control')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('email', 'Email', array('class' => 'control-label col-sm-2')) }}
			<div class="col-sm-4">
				{{ Form::email('email', null, array('class' => 'form-control')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('telephone', 'Telephone', array('class' => 'control-label col-sm-2')) }}
			<div class="col-sm-2">
				{{ Form::text('telephone', null, array('class' => 'form-control')) }}
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-4 col-sm-offset-2">
				{{ Form::submit('Create New Booking', array('class' => 'btn btn-primary')) }}
			</div>
		</div>

	{{ Form::close() }}
@stop