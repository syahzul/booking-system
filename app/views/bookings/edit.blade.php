@extends('master')

@section('content')

	<div class="page-header">
		<h1>Edit Booking</h1>
	</div>

	{{ Form::model($booking, array('route' => array('booking.update', $booking->id), 'class' => 'form-horizontal', 'method' => 'PATCH')) }}
		
		<div class="form-group">
			{{ Form::label('started_at', 'Started at', array('class' => 'control-label col-sm-2')) }}
			<div class="col-xs-3">
				{{ Form::text('started_at', $booking->started_at, array('class' => 'form-control')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('ended_at', 'Ended at', array('class' => 'control-label col-sm-2')) }}
			<div class="col-xs-3">
				{{ Form::text('ended_at', $booking->ended_at, array('class' => 'form-control')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('room_id', 'Room', array('class' => 'control-label col-sm-2')) }}
			<div class="col-sm-4">
				{{ Form::select('room_id', $rooms, $booking->room_id, array('class' => 'form-control')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('name', 'Name', array('class' => 'control-label col-sm-2')) }}
			<div class="col-sm-4">
				{{ Form::text('name', $booking->name, array('class' => 'form-control')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('email', 'Email', array('class' => 'control-label col-sm-2')) }}
			<div class="col-sm-4">
				{{ Form::email('email', $booking->email, array('class' => 'form-control')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('telephone', 'Telephone', array('class' => 'control-label col-sm-2')) }}
			<div class="col-sm-2">
				{{ Form::text('telephone', $booking->telephone, array('class' => 'form-control')) }}
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-4 col-sm-offset-2">
				{{ Form::submit('Update Booking', array('class' => 'btn btn-primary')) }}
			</div>
		</div>

	{{ Form::close() }}
@stop