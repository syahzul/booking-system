@extends('master')

@section('content')

	<div class="page-header">
		<div class="pull-right">
			<a href="{{ URL::route('booking.create') }}" class="btn btn-success">
				Add
			</a>
		</div>

		<h1>Bookings</h1>
	</div>

	<table class="table table-striped table-list">
		<thead>
			<tr>
				<th width="100">Start At</th>
				<th width="100">End At</th>
				<th wisth="140">Room</th>
				<th>Name</th>
				<th>Email</th>
				<th width="70"></th>
				<th width="100"></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($bookings as $booking)
			<tr>
				<td>{{{ $booking->started_at }}}</td>
				<td>{{{ $booking->ended_at }}}</td>
				<td>{{{ $booking->room->name }}}</td>
				<td>{{{ $booking->name }}}</td>
				<td>{{{ $booking->email }}}</td>
				<td>
					<a href="{{ URL::route('booking.edit', $booking->id) }}" class="btn btn-warning">
						Edit
					</a>
				</td>
				<td>
					{{ Form::model($booking, array('route' => array('booking.destroy', $booking->id), 'method' => 'DELETE')) }}
						{{ Form::submit('Delete', array('class' => 'btn btn-danger remove-item')) }}
					{{ Form::close() }}
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@stop