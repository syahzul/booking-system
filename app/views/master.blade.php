<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('page-title', 'Room Booking System')</title>

        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
        <link href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
        <link href="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.css">
    </head>
    <body>
        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainmenu">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ URL::to('/') }}">
                        Room Booking System
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="mainmenu">
                    <ul class="nav navbar-nav navbar-right">
                        @if (Auth::check())
                            <li class="{{ (Request::is('booking*') ? 'active' : '') }}">
                                <a href="{{ URL::route('booking.index') }}">Bookings</a>
                            </li>
                            <li class="{{ (Request::is('room*') ? 'active' : '') }}">
                                <a href="{{ URL::route('room.index') }}">Rooms</a>
                            </li>
                            <li class="{{ (Request::is('user*') ? 'active' : '') }}">
                                <a href="{{ URL::to('user') }}">Users</a>
                            </li>
                            <li>
                                <a href="{{ URL::to('logout') }}">Logout</a>
                            </li>
                        @else
                            <li>
                                <a href="{{ URL::to('register') }}">Register</a>
                            </li>
                            <li>
                                <a href="{{ URL::to('login') }}">Login</a>
                            </li>
                        @endif
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <div class="container">

            @if ($errors->count())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{{ $error }}</div>
                    @endforeach
                </div>
            @endif


            @yield('content')
        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
        <script src="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>
        <script>
            jQuery(document).ready( function($) {

                $('.table-list').DataTable();

                $('[data-toggle="popover"]').popover();

                $('.remove-item').click( function() {
                    if ( ! confirm('Are you sure you want to remove this item?')) {
                        return false;
                    }
                });

            });
        </script>
    </body>
</html>