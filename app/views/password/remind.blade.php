@extends('master')

@section('content')
    <h1>Reset Password</h1>

    <hr>

    <form action="{{ action('RemindersController@postRemind') }}" method="POST">

        <div class="form-group">
            <label class="control-label col-sm-3">Email</label>
            <div class="col-sm-6">
                {{ Form::email('email', null, array('class' => 'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-3 col-sm-offset-3">
                {{ Form::submit('Send Reset Password Link', array('class' => 'btn btn-primary btn-block')) }}
            </div>
        </div>

    </form>
@stop