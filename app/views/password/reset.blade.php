@extends('master')

@section('content')
    <h1>Reset Password</h1>

    <hr>

    <form action="{{ action('RemindersController@postReset') }}" method="POST" class="form-horizontal">

        <div class="form-group">
            <label class="control-label col-sm-3">Email</label>
            <div class="col-sm-6">
                {{ Form::email('email', null, array('class' => 'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-3">Password</label>
            <div class="col-sm-3">
                {{ Form::password('password', array('class' => 'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-3">Repeat Password</label>
            <div class="col-sm-3">
                {{ Form::password('password_confirmation', array('class' => 'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-3 col-sm-offset-3">
                <input type="hidden" name="token" value="{{ $token }}">
                {{ Form::submit('Send Reset Password Link', array('class' => 'btn btn-primary btn-block')) }}
            </div>
        </div>

    </form>
@stop