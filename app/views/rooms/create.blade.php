@extends('master')

@section('content')

	<div class="page-header">
		<h1>Add New Room</h1>
	</div>

	{{ Form::open(array('route' => 'room.store', 'class' => 'form-horizontal', 'files' => true)) }}
		
		<div class="form-group">
			{{ Form::label('name', 'Name', array('class' => 'control-label col-sm-2')) }}
			<div class="col-sm-4">
				{{ Form::text('name', null, array('class' => 'form-control')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('picture', 'Picture', array('class' => 'control-label col-sm-2')) }}

			<div class="col-sm-4">
				{{ Form::file('picture', array('class' => 'form-control')) }}
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-4 col-sm-offset-2">
				{{ Form::submit('Create New Room', array('class' => 'btn btn-primary')) }}
			</div>
		</div>

	{{ Form::close() }}
@stop