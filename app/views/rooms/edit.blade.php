@extends('master')

@section('content')

	<div class="page-header">
		<h1>Edit Room</h1>
	</div>

	{{ Form::model($room, array('route' => array('room.update', $room->id), 'method' => 'PATCH', 'class' => 'form-horizontal', 'files' => true)) }}
		
		<div class="form-group">
			{{ Form::label('name', 'Name', array('class' => 'control-label col-sm-2')) }}
			<div class="col-sm-4">
				{{ Form::text('name', $room->name, array('class' => 'form-control')) }}
			</div>
		</div>

		<div class="form-group">
			{{ Form::label('picture', 'Picture', array('class' => 'control-label col-sm-2')) }}

			<div class="col-sm-4">
				{{ Form::file('picture', array('class' => 'form-control')) }}

				@if (! empty($room->picture))
				<hr>
				<p>Existing picture:</p>
				<div class="thumbnail">
					<img src="{{ URL::asset('images/'.$room->picture) }}">
				</div>
				@endif
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-4 col-sm-offset-2">
				{{ Form::submit('Update Room', array('class' => 'btn btn-primary')) }}
			</div>
		</div>

	{{ Form::close() }}
@stop