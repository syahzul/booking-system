@extends('master')

@section('content')

	<div class="page-header">
		<div class="pull-right">
			<a href="{{ URL::route('room.create') }}" class="btn btn-success">
				Add
			</a>
		</div>

		<h1>Rooms</h1>
	</div>

	<table class="table table-striped table-list">
		<thead>
			<tr>
				<th>Name</th>
				<th width="50">Booking</th>
                <th width="70">Room</th>
				<th width="100"></th>
			</tr>
		</thead>
		<tbody>
			@foreach ($rooms as $room)
                <?php
                $image = '';
                if (! empty($room->picture)) {
                    $image = '<img src="'.URL::asset('images/'.$room->picture).'" width="150">';
                }
                ?>
			<tr data-content="{{{ $image }}}"
                data-html="true"
                data-trigger="hover"
                data-placement="top"
                data-toggle="popover">
				<td>{{{ $room->name }}}</td>
                <td>{{ $room->bookings()->count() }}</td>
				<td>
					<a href="{{ URL::route('room.edit', $room->id) }}" class="btn btn-warning">
						Edit
					</a>
				</td>

				<td>
					{{ Form::model($room, array('route' => array('room.destroy', $room->id), 'method' => 'DELETE')) }}
						{{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
					{{ Form::close() }}
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@stop