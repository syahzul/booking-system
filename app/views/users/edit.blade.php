@extends('master')

@section('content')
    <h1>Update User</h1>

    <hr>

    {{ Form::model($user, array('route' => ['user.edit', $user->id], 'class' => 'form-horizontal', 'method' => 'PATCH')) }}

        <div class="form-group">
            <label class="control-label col-sm-3">Full Name</label>
            <div class="col-sm-6">
                {{ Form::text('name', $user->name, array('class' => 'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-3">Email</label>
            <div class="col-sm-6">
                {{ Form::email('email', $user->email, array('class' => 'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-3">Password</label>
            <div class="col-sm-3">
                {{ Form::password('password', array('class' => 'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-3">Repeat Password</label>
            <div class="col-sm-3">
                {{ Form::password('password_confirmation', array('class' => 'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-3 col-sm-offset-3">
                {{ Form::submit('Update User', array('class' => 'btn btn-primary btn-block')) }}
            </div>
        </div>

    {{ Form::close() }}
@stop