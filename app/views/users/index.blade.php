@extends('master')

@section('content')
    <div class="page-header">
        <div class="pull-right">
            <a href="{{ URL::to('user/create') }}" class="btn btn-success">
                Add
            </a>
        </div>

        <h1>All Users</h1>
    </div>

    <table class="table table-striped table-list">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th width="70"></th>
                <th width="100"></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
            <tr>
                <td>{{{ $user->name }}}</td>
                <td>{{{ $user->email }}}</td>
                <td>
                    <a href="{{ URL::route('user.edit', $user->id) }}" class="btn btn-warning">
                        Edit
                    </a>
                </td>
                <td>
                    {{ Form::model($user, array('route' => array('user.destroy', $user->id), 'method' => 'DELETE')) }}
                        {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                    {{ Form::close() }}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

@stop