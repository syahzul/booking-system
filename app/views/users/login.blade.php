@extends('master')

@section('content')
    <h1>Login to Account</h1>

    <hr>

    {{ Form::open(array('to' => 'login', 'class' => 'form-horizontal')) }}

        <div class="form-group">
            <label class="control-label col-sm-3">Email</label>
            <div class="col-sm-6">
                {{ Form::email('email', null, array('class' => 'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-3">Password</label>
            <div class="col-sm-3">
                {{ Form::password('password', array('class' => 'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-3 col-sm-offset-3">
                {{ Form::submit('Login to Account', array('class' => 'btn btn-primary btn-block')) }}
            </div>
            <div class="col-sm-3">
                <ul class="list-inline">
                    <li class="form-control-static">
                        <a href="{{ URL::to('register') }}">Register</a>
                    </li>
                    <li class="form-control-static">
                        <a href="{{ URL::to('password/remind') }}">Forgot Password</a>
                    </li>
                </ul>
            </div>
        </div>

    {{ Form::close() }}
@stop